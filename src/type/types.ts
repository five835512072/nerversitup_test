export type SerializeType = {
    code: string
    message: string
  }

  export type UserModel = {
    id: number| null,
    user_id: string,
    name: string,
    surname: string,
    password: string,
    create_at: Date,
    update_at: Date | null,
    email: string
  }
  export type RegisterRequestModel = {
    name: string,
    surname: string,
    password: string,
    address: string,
    email: string
  }
  export type LoginRequestModel = {
    password: string,
    email: string
  }

  export type ProductModel = {
    id:number| null,
    name: string,
    detail: string,
    category_id: string,
    create_at: Date,
    update_at: Date | null,
    price: number,
    quantity: number,
    status: string,
    create_by_id: string,
    update_by_id: string,
  }

  export type OrderModel = {
    id: number | null,
    detail: string,
    product_id: number,
    create_at: Date,
    update_at: Date,
    total_price: number,
    quantity: number,
    status: string,
    create_by_id: string,
    update_by_id: string,
  }

  export type CreateOrderRequestModel = {
    detail: string,
    productId: number,
    quantity: number,
    userId: string,
  }
