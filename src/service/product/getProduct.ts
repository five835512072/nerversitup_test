import { products } from "@prisma/client"
import { findProduct } from "../../model/product/find"
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"

export async function getProductService(id: number): Promise<products> {

    const productDetail: products | null =  await findProduct(id)
    if(!productDetail){
        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_ID, BUSINESS_CODE.NOT_FOUND)
    }

    return productDetail
}
