import { products } from "@prisma/client"
import { findAllProduct } from "../../model/product/find"

export async function getAllProductService(): Promise<products[] | null> {

    return await findAllProduct()
}
