import { findUserbyUserId } from "../../model/user/find"
import { UserModel } from "../../type/types"
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"


export async function getUserProfileService(userId: string): Promise<void | UserModel> {
    const userModel :UserModel| null = await findUserbyUserId(userId)

    if(!userModel){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_USERID, BUSINESS_CODE.NOT_FOUND)
    }
    else {

        return userModel
    }
}
