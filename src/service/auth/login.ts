import { findUserbyEMail } from "../../model/user/find"
import { LoginRequestModel, UserModel } from "../../type/types"
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"


export async function loginService(loginModel: LoginRequestModel ): Promise<UserModel> {
    const userModel :UserModel| null = await findUserbyEMail(loginModel.email)

    if(!userModel){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_EMAIL, BUSINESS_CODE.NOT_FOUND)
    }
    else {
        if(userModel.password !== loginModel.password){

            throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_PASSWORD, BUSINESS_CODE.NOT_MATCH)
        }
        else {

            return userModel 
        }      
    }
}
