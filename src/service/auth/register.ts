import { findUserbyEMail } from "../../model/user/find"
import { RegisterRequestModel, UserModel } from "../../type/types"
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"
import { createUsers } from "../../model/user/insert"
import { generateUUID } from "../../utils/uuid"


export async function registerService(registerModel: RegisterRequestModel ): Promise<void> {
    if(!await findUserbyEMail(registerModel.email)){

        const userID : string = await generateUUID()
        const userModel: UserModel ={
            id:null,
            user_id:userID,
            name: registerModel.name,
            surname: registerModel.surname,
            password:registerModel.password,
            create_at:new Date(),
            update_at:null,
            email: registerModel.email
        }
        await createUsers(userModel)
    }
    else {

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_EMAIL, BUSINESS_CODE.FIELD_MANDATORY)
    }

}
