import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"
import { orders } from "@prisma/client"
import { findAllOderByUserId, findOrderById, findOrderByIdAndUserID } from "../../model/order/find"


export async function getOrderService(orderId: number, userId: string): Promise<orders | null> {

    const orderDetail : orders | null = await findOrderById(orderId)

    if(!orderDetail){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_ID, BUSINESS_CODE.NOT_FOUND)
    }

    if(orderDetail.create_by_id !== userId){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_USERID, BUSINESS_CODE.NOT_MATCH)
    }

    return orderDetail
}

export async function getAllOrderByUserIdService(userId: string): Promise<orders[] | null> {
 
    const orderDetail : orders[] | null = await findAllOderByUserId(userId)

    if(orderDetail?.length === 0){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_USERID, BUSINESS_CODE.NOT_FOUND)
    }


    return orderDetail
}