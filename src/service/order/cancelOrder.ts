import { STATUS, BUSINESS_CODE, HTTP_CODE, STATUS_ORDER } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"
import { orders } from "@prisma/client"
import { cancelOrder } from "../../model/order/update"
import { findOrderById } from "../../model/order/find"


export async function cancelOrderService(orderId: number, userId: string): Promise<void> {

    const orderDetail : orders | null = await findOrderById(orderId)

    if(!orderDetail){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_ID, BUSINESS_CODE.NOT_FOUND)
    }

    if(orderDetail.create_by_id !== userId){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_USERID, BUSINESS_CODE.NOT_MATCH)
    }

    if(STATUS_ORDER.PENDING !== orderDetail.status){
        
        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_STATUS, BUSINESS_CODE.NOT_MATCH)
    }

    await cancelOrder(orderId, STATUS_ORDER.CANCEL)
}
