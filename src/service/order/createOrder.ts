import { findUserbyEMail, findUserbyUserId } from "../../model/user/find"
import { CreateOrderRequestModel, OrderModel, RegisterRequestModel, UserModel } from "../../type/types"
import { STATUS, BUSINESS_CODE, HTTP_CODE, STATUS_ORDER } from "../../constant/enum"
import { CheckError} from "../../middleware/errors/errorhandler"
import { findProduct } from "../../model/product/find"
import { products } from "@prisma/client"
import { createOrder } from "../../model/order/insert"


export async function createOrderService(createOrderModel: CreateOrderRequestModel): Promise<void> {

    if(!await findUserbyUserId(createOrderModel.userId)){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_USERID, BUSINESS_CODE.NOT_FOUND)
    }

    const productDetail: products | null =  await findProduct(createOrderModel.productId)

    if(!productDetail){

        throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_PRODUCTID, BUSINESS_CODE.NOT_FOUND)
    }

    const totalPrice: number = createOrderModel.quantity * productDetail.price

    const orderModel: OrderModel ={
        id: null,
        detail: createOrderModel.detail,
        product_id: createOrderModel.productId,
        create_at: new Date(),
        update_at: new Date(),
        total_price: totalPrice,
        quantity: createOrderModel.quantity,
        status: STATUS_ORDER.PENDING,
        create_by_id: createOrderModel.userId,
        update_by_id: createOrderModel.userId,
    }

    await createOrder(orderModel)
}
