import type { Response, Request, NextFunction } from "express"
import { createOrderService } from "../../service/order/createOrder"


export async function createOrder(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {
    return res
      .status(201)
      .json(await createOrderService(req.body))

  } catch (error) {
    next(error)
  }
}
