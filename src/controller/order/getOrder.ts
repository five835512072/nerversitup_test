import type { Response, Request, NextFunction } from "express"
import { createOrderService } from "../../service/order/createOrder"
import { getOrderService } from "../../service/order/getOrder"


export async function getOrder(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {
    return res
      .status(200)
      .json(await getOrderService(Number(req.headers.id), req.headers.userid as string))

  } catch (error) {
    next(error)
  }
}
