import type { Response, Request, NextFunction } from "express"
import { cancelOrderService } from "../../service/order/cancelOrder"


export async function cancelOrder(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {
    return res
      .status(200)
      .json(await cancelOrderService(Number(req.headers.id), req.headers.userid as string))

  } catch (error) {
    next(error)
  }
}
