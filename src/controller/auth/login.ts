import type { Response, Request, NextFunction } from "express"
import { loginService } from "../../service/auth/login"

export async function login(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {

    return res
      .status(200)
      .json(await loginService(req.body))
  } catch (error) {

    next(error)
  }
}
