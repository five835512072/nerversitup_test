import type { Response, Request, NextFunction } from "express"
import { registerService } from "../../service/auth/register"

export async function register(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {
    return res
      .status(201)
      .json(await registerService(req.body))

  } catch (error) {
    next(error)
  }
}
