import type { Response, Request, NextFunction } from "express"
import { getAllOrderByUserIdService } from "../../service/order/getOrder"


export async function getOrdersProfile(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {

    return res
      .status(200)
      .json(await getAllOrderByUserIdService(req.headers.userid as string))
  } catch (error) {

    next(error)
  }
}