import type { Response, Request, NextFunction } from "express"
import { getUserProfileService } from "../../service/user/getUserProfile"

export async function getUserProfile(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {

    return res
      .status(200)
      .json(await getUserProfileService(req.headers.userid as string))
  } catch (error) {

    next(error)
  }
}
