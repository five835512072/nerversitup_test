import type { Response, Request, NextFunction } from "express"
import { getProductService } from "../../service/product/getProduct"


export async function getProduct(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {

    return res
      .status(200)
      .json(await getProductService(Number(req.headers.id)))
  } catch (error) {

    next(error)
  }
}