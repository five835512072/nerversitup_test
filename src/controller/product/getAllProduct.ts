import type { Response, Request, NextFunction } from "express"
import { getAllProductService } from "../../service/product/getAllproduct"


export async function getAllProduct(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
  try {

    return res
      .status(200)
      .json(await getAllProductService())
  } catch (error) {

    next(error)
  }
}
