import { PrismaClient } from '@prisma/client'
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError } from "../../middleware/errors/errorhandler"
import { UserModel } from '../../type/types'

export async function findUserbyEMail(email: string
  ): Promise<UserModel | null> {
    const prisma = new PrismaClient()
  
    if (!prisma) {
      throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
    }

    const userResult: UserModel | null = await prisma.users.findFirst({
        where: {
            email,
          },
      })
      return userResult
  }

  export async function findUserbyUserId(userId: string
    ): Promise<UserModel | null> {
      const prisma = new PrismaClient()
    
      if (!prisma) {
        throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
      }
  
      const userResult: UserModel | null = await prisma.users.findFirst({
          where: {
              user_id:userId,
            },
        })
        return userResult
    }
    