import { PrismaClient } from '@prisma/client'
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError } from "../../middleware/errors/errorhandler"
import { UserModel } from '../../type/types'

export async function createUsers(userModel: UserModel
  ): Promise<void> {
    const prisma = new PrismaClient()
  
    if (!prisma) {
      throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
    }

    await prisma.users.create({
      data: {
        email: userModel.email,
        name: userModel.name,
        surname: userModel.surname,
        user_id: userModel.user_id,
        password: userModel.password,
        create_at: userModel.create_at
      },
    })
  }
