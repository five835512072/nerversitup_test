import { PrismaClient, products } from '@prisma/client'
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError } from "../../middleware/errors/errorhandler"


export async function findAllProduct(
    ): Promise<products[] | null> {
        const prisma = new PrismaClient()
    
        if (!prisma) {
            throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
        }
        const productResult: products[] | null = await prisma.products.findMany()

        return productResult
    }

    export async function findProduct(id:number
        ): Promise<products | null> {
            const prisma = new PrismaClient()
        
            if (!prisma) {
                
                throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
            }
            
            const productResult: products | null = await prisma.products.findFirst({
                where: {
                    id
                  },
              })
    
            return productResult
        }
