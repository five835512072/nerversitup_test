import { PrismaClient } from '@prisma/client'
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError } from "../../middleware/errors/errorhandler"
import { OrderModel, UserModel } from '../../type/types'

export async function createOrder(orderModel: OrderModel
  ): Promise<void> {
    const prisma = new PrismaClient()
  
    if (!prisma) {
      throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
    }

    await prisma.orders.create({
      data: {
        detail: orderModel.detail,
        product_id:orderModel.product_id,
        create_at:orderModel.create_at,
        update_at:orderModel.update_at,
        total_price:orderModel.total_price,
        quantity:orderModel.quantity,
        status:orderModel.status,
        create_by_id:orderModel.create_by_id,
        update_by_id:orderModel.update_by_id,
      },
    })
  }
