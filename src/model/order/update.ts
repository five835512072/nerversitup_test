import { PrismaClient } from '@prisma/client'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError } from "../../middleware/errors/errorhandler"

export async function cancelOrder(id: number, status: string
  ): Promise<void> {
    const prisma = new PrismaClient()
  
    if (!prisma) {
        
      throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
    }

    try{       
        await prisma.orders.update({
            where: {
                id,
            },
            data: {
                status,
            },
        })
    }catch(error){
        if((error as PrismaClientKnownRequestError).code === "P2025"){

            throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_ID, BUSINESS_CODE.NOT_FOUND)
        }
    }
  }
