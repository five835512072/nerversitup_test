import { orders, PrismaClient } from '@prisma/client'
import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { STATUS, BUSINESS_CODE, HTTP_CODE } from "../../constant/enum"
import { CheckError } from "../../middleware/errors/errorhandler"

export async function findOrderById(id: number
  ): Promise<orders | null> {
    const prisma = new PrismaClient()
  
    if (!prisma) {
        
      throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
    }

        const orderDetail: orders | null = await prisma.orders.findFirst({
            where: {
                id,
            }
        })

        return orderDetail
  }

  export async function findOrderByIdAndUserID(id: number, userId: string
    ): Promise<orders | null> {
      const prisma = new PrismaClient()
    
      if (!prisma) {
          
        throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
      }
      try {
          const orderDetail: orders | null = await prisma.orders.findFirst({
              where: {
                  id,
                  create_by_id: userId,
              }
          })
  
          return orderDetail

        }catch(error){

            if((error as PrismaClientKnownRequestError).code === "P2025"){
    
                throw new CheckError(HTTP_CODE.BAD_REQUEST, STATUS.INVALID_ID, BUSINESS_CODE.NOT_FOUND)
            }

            throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
        }
    }

    export async function findAllOderByUserId(userId: string
        ): Promise<orders[] | null> {
          const prisma = new PrismaClient()
        
          if (!prisma) {
              
            throw new CheckError(HTTP_CODE.BAD_GATEWAY, STATUS.CONNECT_DATABASE_ERROR, BUSINESS_CODE.DATABASE_ERROR)
          }

              const ordersDetail: orders[] | null = await prisma.orders.findMany({
                  where: {
                      create_by_id: userId,
                  }
              })
      
              return ordersDetail
    
        }
