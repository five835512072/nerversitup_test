import { v4 as uuidv4 } from 'uuid'

export async function generateUUID(): Promise<string> {

    return uuidv4()
}