import { Request, Response, NextFunction } from "express"
import { validationResult } from "express-validator"
import { CheckError } from "../errors/errorhandler"
import type { SerializeType } from "../../type/types"

export const check_errors = (
  req: Request,
  res: Response,
  next: NextFunction,
): void | Response | SerializeType => {
  const errors = validationResult(req).formatWith(({ msg }) => msg)
  const hasError = !errors.isEmpty()

  if (hasError) {
    const reqError = new CheckError
      (
        errors['errors'][0]["msg"]["httpCode"],
        errors['errors'][0]["msg"]["message"],
        errors['errors'][0]["msg"]["businessCode"]
      )

    return res
      .status(reqError.httpCode)
      .json(reqError.serialize())
  }
  
  next()
}
