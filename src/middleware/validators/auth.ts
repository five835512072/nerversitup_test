import { check, header } from "express-validator"
import { HTTP_CODE, BUSINESS_CODE } from "../../constant/enum"

export const check_req_register = [
  check("name",
  {
    "message": "Invalid name",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).notEmpty(),
  check("surname",
  {
    "message": "Invalid surname",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).notEmpty(),
  check("email",
  {
    "message": "Invalid email",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).isEmail().notEmpty(),
  check("address",
  {
    "message": "Invalid address",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).notEmpty(),
  check("password",
  {
    "message": "Invalid password about length",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).isLength({ min: 8, max:20 }),
  check("password",
  {
    "message": "password must has uppercase",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).matches('[A-Z]'),
  check("password",
  {
    "message": "password must has lowercase",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).matches('[a-z]'),
  check("password",
  {
    "message": "password must has number",
    "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
    "httpCode": HTTP_CODE.BAD_REQUEST
  }).matches('[0-9]'),
]

export const check_req_login = [
    check("email",
    {
      "message": "Invalid email",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).isEmail().notEmpty(),
    check("password",
    {
      "message": "Invalid password about length",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).isLength({ min: 8, max:20 }),
    check("password",
    {
      "message": "password must has uppercase",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).matches('[A-Z]'),
    check("password",
    {
      "message": "password must has lowercase",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).matches('[a-z]'),
    check("password",
    {
      "message": "password must has number",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).matches('[0-9]'),
  ]

  export const check_req_get_profile = [
    header("userid",
    {
      "message": "Invalid UserId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty()
]

export const check_req_get_order_profile = [
    header("userid",
    {
      "message": "Invalid UserId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty()
]

export const check_req_get_product = [
    header("id",
    {
      "message": "Invalid UserId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty().isNumeric()
]

export const check_req_craete_order = [
    check("userId",
    {
      "message": "Invalid UserId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty(),
    check("productId",
    {
      "message": "Invalid productId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty().isNumeric(),
    check("quantity",
    {
      "message": "Invalid quantity",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty().isNumeric(),
    check("detail",
    {
      "message": "Invalid detail",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty()  
]

export const check_req_cancel_order = [
    header("userId",
    {
      "message": "Invalid UserId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty(),
    header("id",
    {
      "message": "Invalid Id",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty().isNumeric(),
]

export const check_req_get_order = [
    header("userId",
    {
      "message": "Invalid UserId",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty(),
    header("id",
    {
      "message": "Invalid Id",
      "businessCode": BUSINESS_CODE.FIELD_MANDATORY,
      "httpCode": HTTP_CODE.BAD_REQUEST
    }).notEmpty().isNumeric(),
]
