import type { Response, Request, NextFunction } from "express"
import { AxiosError } from 'axios'
import { ValidationError } from "express-validator"
import type { SerializeType } from "../../type/types"

interface Serializable {
  httpCode: number
  message: string
  serialize(): SerializeType
}

export class CheckError extends Error implements Serializable {
  public static serviceId: string
  httpCode: number
  businessCode: number
  message: string
  constructor(httpCode: number, message: string, businessCode: number) {
    super()
    this.httpCode = httpCode
    this.message = message
    this.businessCode = businessCode
  }
  serialize() {
    return {
      code: `${CheckError.serviceId}${this.businessCode}`,
      message: this.message
    }
  }
}

export class MicroserviceError extends Error implements Serializable {
  serviceId: number
  httpCode: number
  message: string 
  constructor(error: AxiosError) {
    super()
    this.httpCode = error.response?.status as number
    this.message = error.response?.data['message']
    this.serviceId = error.response?.data['code']
  }
  serialize() {
    return {
      code: `${this.serviceId}`,
      message: this.message
    }
  }
}

export function errorHandler(error: Serializable | Error, req: Request, res: Response, next: NextFunction) {
  try {
    const tempError = error as Serializable

    if (tempError) {
      res.status(tempError.httpCode).json(tempError.serialize())
    } else {
      res.status(500).json((error as Error).message)
    }
    
  } catch (error) {
    res.status(500).json((error as Error).message)
  }
}
