export enum BUSINESS_CODE {
    INVALID_FIELD = 465,
    FIELD_MANDATORY = 461,
    VARIABLE_ALREDY = 466,
    DIR_EMPTY = 469,
    DATABASE_ERROR = 445,
    INVALID_MODEL_STATE = 419,
    UNEXPECTED_BEHAVIOR = 500,
    INVALID_TOKEN = 412,
    TOKEN_EXPIRED = 411,
    NOT_FOUND = 413,
    NOT_MATCH = 414,

}

export enum HTTP_CODE {
    INTERNAL_SERVER_ERROR = 500,
    BAD_GATEWAY = 502,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    OK = 200,
    NO_CONTENT = 204,
}

export enum STATUS_ORDER{
    PENDING = "pending",
    APPORVE = "approve",
    CANCEL= "cancel",
    SUCCESS = "success"
}

export enum STATUS {
    INVALID_EMAIL = 'Invalid email',
    INVALID_PASSWORD = 'Invalid password',
    INVALID_USERID = 'Invalid user ID',
    INVALID_PRODUCTID = 'Invalid product ID',
    INVALID_ID = 'Invalid ID',
    INVALID_STATUS = 'Invalid status',
    CONNECT_DATABASE_ERROR = 'connect database error',
}
