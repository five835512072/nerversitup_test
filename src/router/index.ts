import express from "express"
import v1Collection from "./v1/index"

const routers = express.Router()

routers.use("/v1", v1Collection)

export default routers