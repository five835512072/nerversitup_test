import express from "express"
import { check_errors } from "../../middleware/validators/common"
import { check_req_get_order_profile, check_req_get_profile } from "../../middleware/validators/auth"
import { getUserProfile } from "../../controller/User/getUserProfile"
import { getOrdersProfile } from "../../controller/User/getOrderhistory"


const router = express.Router()

router.get(
    "/getprofile",
    check_req_get_profile,
    check_errors,
    getUserProfile,
  )

  router.get(
    "/getordersprofile",
    check_req_get_order_profile,
    check_errors,
    getOrdersProfile,
  )


export default router