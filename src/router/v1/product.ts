import express from "express"
import { getAllProduct } from "../../controller/product/getAllProduct"
import { getProduct } from "../../controller/product/getProduct"
import { check_errors } from "../../middleware/validators/common"
import { check_req_get_product } from "../../middleware/validators/auth"


const router = express.Router()

router.get(
    "/getallproduct",
    getAllProduct,
  )

  router.get(
    "/getproduct",
    check_req_get_product,
    check_errors,
    getProduct,
  )

export default router
