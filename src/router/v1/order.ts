import express from "express"
import { check_errors } from "../../middleware/validators/common"
import { check_req_cancel_order, check_req_craete_order, check_req_get_order } from "../../middleware/validators/auth"
import { createOrder } from "../../controller/order/createOrder"
import { cancelOrder } from "../../controller/order/cancelOrder"
import { getOrder } from "../../controller/order/getOrder"


const router = express.Router()

  router.post(
    "/createorder",
    check_req_craete_order,
    check_errors,
    createOrder,
  )

  router.patch(
    "/cancelorder",
    check_req_cancel_order,
    check_errors,
    cancelOrder,
  )

  router.get(
    "/getorder",
    check_req_get_order,
    check_errors,
    getOrder,
  )

export default router
