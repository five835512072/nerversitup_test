import express from "express"
import Auth from "./auth"
import User from "./user"
import Product from "./product"
import Order from "./order"


const routers = express.Router()

routers.use("/auth", Auth)
routers.use("/user", User)
routers.use("/product", Product)
routers.use("/order", Order)

export default routers
