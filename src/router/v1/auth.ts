import express from "express"
import { register } from "../../controller/auth/register"
import { check_errors } from "../../middleware/validators/common"
import { check_req_login, check_req_register } from "../../middleware/validators/auth"
import { login } from "../../controller/auth/login"


const router = express.Router()

router.post(
    "/register",
    check_req_register,
    check_errors,
    register,
  )

  router.post(
    "/login",
    check_req_login,
    check_errors,
    login,
  )

export default router