import dotenv from "dotenv"
import express from "express"
import routers from "./router"
import { errorHandler, CheckError } from "./middleware/errors/errorhandler"


const app = express()
app.use(express.json())

dotenv.config()

 app.use("/", routers)
 app.use(errorHandler)

app.listen(process.env.NODE_PORT, () => console.log("running ->" + process.env.NODE_PORT))

CheckError.serviceId = process.env.SERVICE_ID as string

export default app